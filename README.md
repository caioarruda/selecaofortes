# SelecaoFortes

http://34.192.152.217:8080    Front
<br/>
http://3.89.138.158:5000/api  API
<br>
<br>
# Script do BD
create database simulador;<br>
use simulador;<br>
CREATE TABLE users (<br>
    id INT PRIMARY KEY IDENTITY (1, 1),<br>
    username VARCHAR (50) NOT NULL,<br>
    pass VARCHAR (1000) NOT NULL,<br>
    fullname VARCHAR(250),<br>
    type VARCHAR(20)<br>
);<br>
CREATE TABLE simulations (<br>
    id INT PRIMARY KEY IDENTITY (1, 1),<br>
    userid INT NOT NULL,
    value DECIMAL(10,2) NOT NULL DEFAULT 0,<br>
    tax DECIMAL(10,4) NOT NULL DEFAULT 0,<br>
    quantity INT NOT NULL DEFAULT 1,<br>
    buydate DATETIME NOT NULL,<br>
    FOREIGN KEY (userid) REFERENCES users (id)<br>
);<br>