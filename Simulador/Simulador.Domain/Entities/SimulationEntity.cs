﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulador.Domain.Entities
{
    public class Simulation : BaseEntity
    {
        public decimal Value { get; set; }

        public decimal Tax { get; set; }

        public int Quantity { get; set; }

        public DateTime BuyDate { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

    }
}
