﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simulador.Domain.Entities
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }

        public string Pass { get; set; }

        public string FullName { get; set; }

        public string Type { get; set; }

        public virtual IEnumerable<Simulation> Simulations { get; set; }

    }
}
