﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Simulador.Domain.Entities;
using Simulador.Service.Services;
using Simulador.Service.Validators;

namespace Simulador.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SimulationController : Controller
    {
        private BaseService<Simulation> service = new BaseService<Simulation>();


        [HttpPost]
        [ActionName("Post")]
        public IActionResult Post([FromBody] Simulation item)
        {
            try
            {
                service.Post<SimulationValidator>(item);

                return  new ObjectResult(service.Get(item.Id));
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpPut]
        [ActionName("Put")]
        public IActionResult Put([FromBody] Simulation item)
        {
            try
            {
                service.Put<SimulationValidator>(item);

                return new ObjectResult(item);
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpDelete("{id}")]
        [ActionName("Delete")]
        public IActionResult Delete(int id)
        {
            try
            {
                service.Delete(id);

                return new NoContentResult();
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpGet("{id}")]
        [ActionName("List")]
        public IActionResult GetForUser(int id)
        {
            try
            {
                return new ObjectResult(service.Get().Where(x=> x.UserId == id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpGet("{id}")]
        [ActionName("LastFive")]
        public IActionResult LastFive(int id)
        {
            try
            {
                return new ObjectResult(service.Get().Where(x => x.UserId == id).OrderByDescending(x=> x.Id).Take(5));
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        [ActionName("Get")]
        public IActionResult Get(int id)
        {
            try
            {
                return new ObjectResult(service.Get(id));
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}