import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Simulation, User } from '@/_models';
import { AlertService,  SimulationService, UserService, AuthenticationService } from '@/_services';


@Component({ templateUrl: 'new.component.html' })
export class NewComponent implements OnInit {
    newForm: FormGroup;
    loading = false;
    submitted = false;
	user: User;
	payments: Array<Object>;
    constructor(
        private formBuilder: FormBuilder,
		private simulationService: SimulationService,
        private alertService: AlertService,
		private authenticationService: AuthenticationService,
		private route: ActivatedRoute
    ) {
        this.user = <any>this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.newForm = this.formBuilder.group({
			userid: [''],
            value: ['', Validators.required],
            tax: ['', Validators.required],
            quantity: ['', Validators.required],
            buydate: ['', Validators.required],
        });
		this.route.params.subscribe(params => {
			const id = <string>params['id'];
			if(id != null){
				this.view(id);
			}
		});
    }
	label = "";
	total  = 0.0;
	get f() { return this.newForm.controls; }

    onSubmit() {
		
		this.submitted = true;
        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.newForm.invalid) {
            return;
        }

        this.loading = true;
		this.newForm.value.userid = this.user.id;
        this.simulationService.register(this.newForm.value)
            .pipe(first())
            .subscribe(
                data => {
					this.payments = this.simulationService.calculate(this.newForm.value);
					this.label = "Total(J+V) = ";
					this.total = this.payments[0]["pay"] * this.newForm.value.quantity;
                    this.alertService.success('Simulação registrada com Sucesso!', true);
					this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
	view(id)
	{
		this.simulationService.getId(id).subscribe(x=> {
			console.log(x);
			this.payments = this.simulationService.calculate(x);
		});
		
		
	}
}
