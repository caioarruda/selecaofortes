﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Simulation } from '@/_models';
import { SimulationService } from '@/_services';

@Component({ templateUrl: 'list.component.html' })
export class ListComponent implements OnInit { 
	sims = []; 
    constructor(
		private simulationService: SimulationService
    ) {
        
		
    }
	refresh()
	{
		this.simulationService.getAll().subscribe(x => this.sims = x);
	}
    ngOnInit() {
		this.refresh();
    }
	delete(id){
		this.simulationService.delete(id).subscribe(x=> this.refresh());
	}
}
