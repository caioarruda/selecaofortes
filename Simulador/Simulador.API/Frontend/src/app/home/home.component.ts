import { Component, AfterViewInit } from '@angular/core';
import {  OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Simulation } from '@/_models';
import { SimulationService } from '@/_services';
import * as Prism from 'prismjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit, OnInit {

  /**
   * @method ngAfterViewInit
   */
  ngAfterViewInit() {
    Prism.highlightAll();
  }
  sims = []; 
    constructor(
		private simulationService: SimulationService
    ) {
        
		
    }
	refresh()
	{
		this.simulationService.lastFive().subscribe(x => this.sims = x);
	}
    ngOnInit() {
		this.refresh();
    }
	delete(id){
		this.simulationService.delete(id).subscribe(x=> this.refresh());
	}
}

