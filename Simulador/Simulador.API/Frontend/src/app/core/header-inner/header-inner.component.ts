import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@/_services';
import { User } from '@/_models';
@Component({
  selector: 'app-header-inner',
  templateUrl: './header-inner.component.html'
})
export class HeaderInnerComponent {
	
	public user : User;
	
	  constructor(private authenticationService: AuthenticationService,private router: Router,) {
	  this.user = <any>this.authenticationService.currentUserValue;
	  
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
