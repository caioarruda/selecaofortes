import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'angular-admin-lte';
import { Router } from '@angular/router';
import { AuthenticationService } from './_services';
import { User } from '@/_models';

import './_content/app.less';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
  public customLayout: boolean;
  
  currentUser: User;
  constructor(
    private layoutService: LayoutService,private router: Router,
        private authenticationService: AuthenticationService
  ) {
	  this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

    
  ngOnInit() {
   this.layoutService.isCustomLayout.subscribe((value: boolean) => {
      this.customLayout = value;
    });	  
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
