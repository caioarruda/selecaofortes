export const adminLteConf = { 
  skin: 'blue',
  // isSidebarLeftCollapsed: false,
  // isSidebarLeftExpandOnOver: false,
  // isSidebarLeftMouseOver: false,
  // isSidebarLeftMini: true,
  // sidebarRightSkin: 'dark',
  // isSidebarRightCollapsed: true,
  // isSidebarRightOverContent: true,
  // layout: 'normal',
  sidebarLeftMenu: [
    {label: 'MAIN NAVIGATION', separator: true},
    {label: 'Painel', route: '/', iconClasses: 'fa fa-dashboard', pullRights: [{text: '+', classes: 'label pull-right bg-green'}]},
    {label: 'Simulações', iconClasses: 'fa fa-th-list', children: [
        {label: 'Nova Simulação', route: '/simulation/new'},
        {label: 'Histórico', route: '/simulation/list'}
      ]},
    {label: '', separator: true},
    {label: 'Sair', route: '/logout', iconClasses: 'fa fa-close'}
  ]
};
