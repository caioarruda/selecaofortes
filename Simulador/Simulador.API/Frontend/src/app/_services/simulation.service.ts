import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Global } from '@/_globals';
import { AuthenticationService } from '@/_services/authentication.service';
import { User, Simulation } from '@/_models';
  
@Injectable({ providedIn: 'root' })
export class SimulationService { 
	user : User;
    constructor(private http: HttpClient, public global: Global, 
		private authenticationService: AuthenticationService) { 
		this.user = <any>this.authenticationService.currentUserValue;
	}
	
    getAll() {
		var id = this.user.id;
        return this.http.get<any>(`${this.global.apiUrl}/Simulation/list/`+id).pipe(map(sim => {
			
			sim.forEach(function (value) {
			  var a = Number(value.value);
			  var n =  Number(value.quantity);
			  var i = Number(value.tax)/100;
			  var m = (i/(1.0-Math.pow(1.0+i,-1*n)))*a;
			  value.pay = m.toFixed(2);
			  value.amount = (m*n).toFixed(2);
			});
				console.log(sim);
				return sim;
		}));
    }
	lastFive() {
		var id = this.user.id;
        return this.http.get<any>(`${this.global.apiUrl}/Simulation/lastfive/`+id).pipe(map(sim => {
			
			sim.forEach(function (value) {
			  var a = Number(value.value);
			  var n =  Number(value.quantity);
			  var i = Number(value.tax)/100;
			  var m = (i/(1.0-Math.pow(1.0+i,-1*n)))*a;
			  value.pay = m.toFixed(2);
			  value.amount = (m*n).toFixed(2);
			});
				console.log(sim);
				return sim;
		}));
    }
	getId(id: number) {
        return this.http.get<any>(`${this.global.apiUrl}/Simulation/get/${id}`).pipe(map(sim => {
			var a = Number(sim.value);
			var n =  Number(sim.quantity);
			var i = Number(sim.tax)/100;
			var m = (i/(1.0-Math.pow(1.0+i,-1*n)))*a;
			sim.pay = m.toFixed(2);
			sim.amount = (m*n).toFixed(2);
			sim.buydate = sim.buyDate.toString();
			return sim;   
		}));
		
    } 

    register(simulation: Simulation) {
		return this.http.post<any>(`${this.global.apiUrl}/Simulation/post`, simulation)
            .pipe(map(sim => {
				if(sim.id <= 0)
				{
					return null;
				}
                return sim; 
            }));
    }

	public calculate(sim : Simulation) 
	{
		var a = Number(sim.value);
		var n =  Number(sim.quantity);
		var i = Number(sim.tax)/100;
		var m = (i/(1.0-Math.pow(1.0+i,-1*n)))*a;
		var payments = Array();
        for (var index = 0; index < n; index++) {
			var date = new Date(sim.buydate);
			date = new Date(date.setMonth(date.getMonth() + index + 1));  
		    payments[index] = {
			  "n": String(index+1), 
			  "date": String(date), 
			  "pay": String(m.toFixed(2)), 
			  "amount": String(((m*n)-(m*(index+1))).toFixed(2))
			  };
			  console.log(date);
		}
		return payments;
	}

    delete(id: number) {
        return this.http.delete(`${this.global.apiUrl}/Simulation/delete/${id}`);
    }
}
