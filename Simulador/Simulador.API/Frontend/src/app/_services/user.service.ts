﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Global } from '@/_globals';
import { User } from '@/_models';
  
@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient, public global: Global) { }

    getAll() {
        return this.http.get<User[]>(`${this.global.apiUrl}/user/list`);
    }

    register(user: User) {
        return this.http.post(`${this.global.apiUrl}/user/post`, user);
    }

    delete(id: number) {
        return this.http.delete(`${this.global.apiUrl}/user/delete/${id}`);
    }
}