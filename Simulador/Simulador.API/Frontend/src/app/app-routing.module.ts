import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register';
import { AuthGuard } from './_helpers';
import { NewComponent } from '@/simulations';
import { ListComponent } from '@/simulations';
const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'logout', component: LogoutComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'simulation/new', component: NewComponent },
    { path: 'simulation/new/:id', component: NewComponent },
    { path: 'simulation/list', component: ListComponent },
	
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
