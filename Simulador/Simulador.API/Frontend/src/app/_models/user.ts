import {Simulation} from '@/_models';
export interface User {
    id: string;
    username: string;
	fullname: string;
	type: string;
	token: string;
	simulations: Array<Simulation>
}
