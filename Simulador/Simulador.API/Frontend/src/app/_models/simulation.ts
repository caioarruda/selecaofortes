import {User} from '@/_models';
export interface Simulation {
    value: number;
    tax: number;
	quantity: number;
	buydate: Date;
	userid: number;
	user: User;
	pay: number;
	amount: number;
}
