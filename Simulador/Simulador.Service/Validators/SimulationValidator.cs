﻿using FluentValidation;
using Simulador.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Simulador.Service.Validators
{
    public class SimulationValidator : AbstractValidator<Simulation>
    {
        public SimulationValidator()
        {
            RuleFor(c => c)
                    .NotNull()
                    .OnAnyFailure(x =>
                    {
                        throw new ArgumentNullException("Objeto Não encontrado");
                    });

            RuleFor(c => c.Value)
                .NotEmpty().WithMessage("Necessário Informar o Valor total.")
                .NotNull().WithMessage("Necessário Informar o Nome de usuário.");

            RuleFor(c => c.Tax)
                .NotEmpty().WithMessage("Necessário Informar a Taxa de juros.")
                .NotNull().WithMessage("Necessário Informar a Taxa de juros.");

            RuleFor(c => c.Quantity)
                .NotEmpty().WithMessage("Necessário Informar a quantidade de parcelas.")
                .NotNull().WithMessage("Necessário Informar a quantidade de parcelas.");

            RuleFor(c => c.BuyDate)
                .NotEmpty().WithMessage("Necessário Informar a data da compra.")
                .NotNull().WithMessage("Necessário Informar a data da compra.");
        }
    }
}
