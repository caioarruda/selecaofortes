﻿using FluentValidation;
using Simulador.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Simulador.Service.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(c => c)
                    .NotNull()
                    .OnAnyFailure(x =>
                    {
                        throw new ArgumentNullException("Objeto Não encontrado");
                    });

            RuleFor(c => c.UserName)
                .NotEmpty().WithMessage("Necessário Informar o Nome de usuário.")
                .NotNull().WithMessage("Necessário Informar o Nome de usuário.");

            RuleFor(c => c.Pass)
                .NotEmpty().WithMessage("Necessário Informar a senha.")
                .NotNull().WithMessage("Necessário Informar a senha.");

            RuleFor(c => c.FullName)
                .NotEmpty().WithMessage("Necessário Informar o nome completo.")
                .NotNull().WithMessage("Necessário Informar o nome completo.");
        }
    }
}
