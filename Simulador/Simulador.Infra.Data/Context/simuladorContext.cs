﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Simulador.Domain.Entities;
using Simulador.Infra.Data.EntityConfig;

namespace Simulador.Infra.Data.Context
{
    public partial class SimuladorContext : DbContext
    {
        public SimuladorContext()
        {
        }

        public SimuladorContext(DbContextOptions<SimuladorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Simulation> Simulations { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=database-1.cxz8ga0wgkt3.us-east-1.rds.amazonaws.com,1521;Initial Catalog=simulador;User ID=admin;Password=00258456;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(new UserConfig().Configure);
            modelBuilder.Entity<Simulation>(new SimulationConfig().Configure);
        }
    }
}
