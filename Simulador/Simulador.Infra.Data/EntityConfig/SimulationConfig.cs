﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Simulador.Domain.Entities;


namespace Simulador.Infra.Data.EntityConfig
{
    public class SimulationConfig : IEntityTypeConfiguration<Simulation>
    {
        public void Configure(EntityTypeBuilder<Simulation> builder)
        {
            builder.ToTable("simulations");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Value)
                .IsRequired()
                .HasColumnName("value");

            builder.Property(c => c.Tax)
                .IsRequired()
                .HasColumnName("tax");

            builder.Property(c => c.Value)
                .IsRequired()
                .HasColumnName("value");

            builder.Property(c => c.Quantity)
                .IsRequired()
                .HasColumnName("quantity");

            builder.Property(c => c.BuyDate)
                .IsRequired()
                .HasColumnName("buydate");

            builder.Property(c => c.UserId)
                .IsRequired()
                .HasColumnName("userid");
        }
    }
}
