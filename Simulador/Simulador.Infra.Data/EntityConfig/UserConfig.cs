﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Simulador.Domain.Entities;


namespace Simulador.Infra.Data.EntityConfig
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("users");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.UserName)
                .IsRequired()
                .HasColumnName("username");

            builder.Property(c => c.Pass)
                .IsRequired()
                .HasColumnName("pass");

            builder.Property(c => c.FullName)
                .IsRequired()
                .HasColumnName("fullname");

            builder.Property(c => c.Type)
                .HasColumnName("type");
        }
    }
}
